# Jobcentre Location

## Summary 

Claimants need to know the address of their nearest jobcentre.  This application provides an externally-accessible API to return the address of the nearest jobcentre by given postcode. 

## Installation

