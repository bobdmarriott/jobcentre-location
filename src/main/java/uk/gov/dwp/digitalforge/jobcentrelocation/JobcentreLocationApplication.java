package uk.gov.dwp.digitalforge.jobcentrelocation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring application main class.
 */
@SpringBootApplication
public class JobcentreLocationApplication {

    /**
     * Jobcentre location application run as a Spring application
     *
     * @param args String[]
     */
    public static void main(String[] args) {
        SpringApplication.run(JobcentreLocationApplication.class, args);
    }

}
