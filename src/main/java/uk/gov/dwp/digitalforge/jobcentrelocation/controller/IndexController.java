package uk.gov.dwp.digitalforge.jobcentrelocation.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Web controller for example pages.
 */
@Controller
public class IndexController {

    private static final String VIEW_SEARCH = "pages/search";

    @RequestMapping(value = {"", "/"})
    public String search(Model model) {
        model.addAttribute("activeTab","search");
        return VIEW_SEARCH;
    }

}
