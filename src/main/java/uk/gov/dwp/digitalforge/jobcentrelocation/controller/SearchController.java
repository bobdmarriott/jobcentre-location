package uk.gov.dwp.digitalforge.jobcentrelocation.controller;

import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;

/**
 * RestController for searching for occupation codes.
 */
@RestController
@RequestMapping("/address")
@Api(value = "address", description = "Search operations for ONS AddressBase data")
public class SearchController {

    private static final Logger log = LoggerFactory.getLogger(SearchController.class);
    private static final int FULL_POSTCODE_MIN_LENGTH = 6;
    private static final int PARTIAL_POSTCODE_MIN_LENGTH = 4;

    @Value("${codepointopen.areacodes.file}")
    private String codePointOpenAreaCodesFile;

    @Value("${codepointopen.admincodes.file}")
    private String codePointOpenAdminCodesFile;

    @Autowired
    private Environment environment;

    private String activeProfile;

    @PostConstruct
    private void init() {
        initActiveProfile();
        initCaches();
    }

    private void initActiveProfile() {
        String[] activeProfiles = environment.getActiveProfiles();
        activeProfile = activeProfiles[0];
    }

    private void initCaches() {
        initCodePointAreaCodeCache();
    }

    private void initCodePointAreaCodeCache() {
        try {
//            InputStream inputStream = new ClassPathResource(codePointOpenAreaCodesFile).getInputStream();
//            BufferedReader in = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
//
//            in.readLine();
//            String areaCodesLine;
//
//            while ((areaCodesLine = in.readLine()) != null) {
//                String[] areaCodeRecord = areaCodesLine.split(",");
//                codePointAreaCodeCache.addAreaCode(areaCodeRecord[0], areaCodeRecord[1]);
//            }
//
//            in.close();
        } catch (Exception e) {
            log.error("Error in initCodePointAreaCodeCache: " + e.getMessage());
        }
    }

}
