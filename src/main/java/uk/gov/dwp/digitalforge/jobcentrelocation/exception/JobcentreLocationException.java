package uk.gov.dwp.digitalforge.jobcentrelocation.exception;

/**
 * Custom exception for the JobcentreLocation application.
 */
public class JobcentreLocationException extends Exception {

    /**
     * Custom exception to encapsulate child exceptions
     *
     * @param cause Throwable
     */
    public JobcentreLocationException(Throwable cause) {
        super(cause);
    }
}
