package uk.gov.dwp.digitalforge.jobcentrelocation.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Utility class for reading latlong and postcode information for Jobcentres.
 */
@Component
public class JobcentreLocationUtil {

    private static final Logger log = LoggerFactory.getLogger(uk.gov.dwp.digitalforge.jobcentrelocation.util.JobcentreLocationUtil.class);

    private boolean jobcentreLocationEnabled = true;

    @Value("${jobcentre.postcode.file}")
    private String jobcentrePostcodeFile;

    public void initJobcentreLocationFile() {
        if (jobcentreLocationEnabled) {
            readJobcentreLatLongFile();
//            writeJobcentreOutcodeMappingFile();
        }
    }

    private void readJobcentreLatLongFile() {
        String jobcentreLine = "";

        try {
            InputStream inputStream = new ClassPathResource(jobcentrePostcodeFile).getInputStream();
            BufferedReader in = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));

            while ((jobcentreLine = in.readLine()) != null) {
                String[] jobcentreRecord = jobcentreLine.split("\t");
                String nomisCode = jobcentreRecord[0];
                String jobcentreName = jobcentreRecord[1];
                String jobcentreUprn = jobcentreRecord[2];
                String displayAddress = jobcentreRecord[3];
                String latitude = jobcentreRecord[4];
                String longitude = jobcentreRecord[5];
            }

            in.close();
        } catch (Exception e) {
            log.error("Error in readJobcentreLatLongFile: " + jobcentreLine + e.getMessage());
        }
    }

    private void writeJobcentreOutcodeMappingFile() {
        try {
            Path newFilePath = Paths.get("src/main/resources/jobcentreOutcodeMapping.txt");
            Files.createFile(newFilePath);
            PrintWriter printWriter = new PrintWriter("src/main/resources/jobcentreOutcodeMapping.txt");

//            for (String jobcentreLatLongLine : jobcentreLatLongList) {
//                printWriter.println(jobcentreLatLongLine);
//            }

            printWriter.close();
        } catch (Exception e) {
            log.error("Error in writeJobcentreOutcodeMappingFile: " + e.getMessage());
        }
    }
}
