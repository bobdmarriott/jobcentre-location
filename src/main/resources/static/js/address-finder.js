/*!
 * Script for following pages:
 * search.html
 */
$(function () {

    /**
     * Array to hold AddressAndLocationRecord data for google maps postcode population
     * @type {Array}
     */
    var addressAndLocations = [];

    /**
     * Get list of AddressAndLocationRecord by value in postcode input field.
     */
    function getPostcodeLocations() {
        var isMapVisible = $("#map").is(":visible");
        var isPostcodeValid = $("#partialPostcode").val().length > 5;

        if (isMapVisible && isPostcodeValid) {
            $.getJSON(BASE_URL + "/address/postcodelocation/" + $("#partialPostcode").val().replace(/ /g, '').toUpperCase(),
                setPostcodeMarkers);
        }
    }

    /**
     * Get number of addresses found by postcode sector.
     * @param postcodesector
     */
    function renderAddressCount(postcodesector) {
        $.getJSON(
            BASE_URL + "/address/count/" + postcodesector,
            function (count) {
                $("#numAddressesFound").text(count + " addresses found");
            });
    }

    /**
     * Get AddressBaseRecord by uprn and populate read-only fields.
     * @param uprn
     */
    function renderLocation(uprn) {
        $.getJSON(
            BASE_URL + "/address/addressBase/" + uprn,
            function (json) {
                $("#easting").val(json.coordinateX);
                $("#northing").val(json.coordinateY);
                $("#latitude").val(json.latitude);
                $("#longitude").val(json.longitude);
                renderClassification(json.classificationCode);
                renderCustodian(json.custodianCode);
            });
    }

    /**
     * Get ClassificationScheme by classificationCode and populate classification textarea.
     * @param classificationCode
     */
    function renderClassification(classificationCode) {
        $.getJSON(
            BASE_URL + "/address/classificationScheme/" + classificationCode,
            function (json) {
                $("#classification").val(json.displayClassification.replace(/, /g, "\n"));
            });
    }

    /**
     * Get OrganisationRecord by uprn and populate organisation textarea.
     * @param uprn
     */
    function renderOrganisation(uprn) {
        // Reset organisation value
        $("#organisation").val("");

        $.getJSON(
            BASE_URL + "/address/organisation/" + uprn,
            function (json) {
                $("#organisation").val(json.displayOrganisation.replace(/, /g, "\n"));
            });
    }

    /**
     * Get Custodian by custodianCode and populate custodian textarea.
     * @param custodianCode
     */
    function renderCustodian(custodianCode) {
        // Reset custodian value
        $("#custodian").val("");

        $.getJSON(
            BASE_URL + "/address/custodian/" + custodianCode,
            function (json) {
                $("#custodian").val(json.displayCustodian.replace(/, /g, "\n"));
            });
    }

    /**
     * Get Authority by postcode and populate authority textarea.
     * @param postcode
     */
    function renderAuthority() {
        var postcode = $("#partialPostcode").val().replace(/ /g, '').toUpperCase();
        // Reset authority value
        $("#authority").val("");

        $.getJSON(
            BASE_URL + "/address/authority/" + postcode,
            function (json) {
                $("#authority").val(json.displayAuthority.replace(/, /g, "\n"));
            });
    }

    /**
     * Get list of DeliveryPointAddressRecord by postcode input.
     * Strip out spaces and convert to uppercase in REST GET request.
     * @param request
     * @param response
     */
    var searchPartialPostcode = function (request, response) {
        var postcodesector = request.term.replace(/ /g, '').toUpperCase();
        renderAddressCount(postcodesector);

        $.getJSON(
            BASE_URL + "/address/postcodesector/" + postcodesector,
            function (data) {
                response(data);
            });
    }

    /**
     * Display google map and overlay with location markers using addressAndLocations array.
     */
    function updateGoogleMap() {
        var latitude = $("#latitude").val();
        var longitude = $("#longitude").val();

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 17,
            center: new google.maps.LatLng(latitude, longitude),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var infowindow = new google.maps.InfoWindow();
        var marker, i;

        for (i = 0; i < addressAndLocations.length; i++) {
            if (isSelectedAddress(addressAndLocations[i][0])) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(addressAndLocations[i][1], addressAndLocations[i][2]),
                    map: map,
                    zIndex: 1
                });
            } else {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(addressAndLocations[i][1], addressAndLocations[i][2]),
                    map: map,
                    label: ' ',
                    opacity: 0.5
                });
            }

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent(addressAndLocations[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }
    }

    /**
     * Check if given value matches that in the address textarea with line breaks replaced with HTML breaks.
     * @param displayAddress
     * @returns {boolean}
     */
    function isSelectedAddress(displayAddress) {
        var selectedAddress = $("#address").val().replace(/\n/g, "<br/>");
        return displayAddress == selectedAddress;
    }

    /**
     * Populate the addressAndLocations array from the returned AddressAndLocationRecord list.
     * @param data
     */
    function setPostcodeMarkers(data) {
        var postcodeLocations = [];

        data.forEach(function (postcode) {
            var aDisplayAddress = postcode.displayAddress.replace(/, /g, "<br/>");
            var aLatitude = postcode.latitude;
            var aLongitude = postcode.longitude;
            var aLocation = [aDisplayAddress, aLatitude, aLongitude];
            postcodeLocations.push(aLocation);
        });

        addressAndLocations = postcodeLocations;
        updateGoogleMap();
    }

    /**
     * Return a text value with given join text
     * @param textValue
     * @param joinText
     * @returns {*}
     */
    function checkTextValue(textValue, joinText) {
        if (textValue) {
            return textValue + joinText;
        } else {
            return "";
        }
    }

    /**
     * Retrieve value from postcode text input.
     * @param item
     * @returns {*}
     */
    function renderPostcode(item) {
        var postcode = checkTextValue(item.postcode, "");
        return postcode;
    }

    /**
     * Populate read-only input fields dependent on selection from partialPostcode auto-complete field.
     * @param event
     * @param ui
     * @returns {boolean}
     */
    var selectPartialPostcode = function (event, ui) {
        var postcode = renderPostcode(ui.item);
        $("#partialPostcode").val(postcode);
        $("#address").val(ui.item.displayAddress.replace(/, /g, "\n"));
        $("#uprn").val(ui.item.uprn);
        $("#udprn").val(ui.item.udprn);
        renderAddressCount(postcode.replace(/ /g, '').toUpperCase());
        renderLocation(ui.item.uprn);
        renderOrganisation(ui.item.uprn);
        renderAuthority();
        getPostcodeLocations();
        return false;
    }

    /**
     * Auto-complete renderer based on partialPostcode input text.
     * @param ul
     * @param item
     * @returns {*|jQuery}
     * @private
     */
    $("#partialPostcode").autocomplete({
        source: searchPartialPostcode,
        select: selectPartialPostcode,
        minLength: 4
    })
        .autocomplete("instance")._renderItem = function (ul, item) {
        return $("<li>")
            .append("<div>" + item.displayAddress + "</div>")
            .append("</li>")
            .appendTo(ul);
    }

    /**
     * googlemap toggle listener.
     */
    $('#googlemap').change(function () {
        var isShowMap = $(this).prop('checked');

        if (isShowMap) {
            $("#map").show();
            getPostcodeLocations();
        } else {
            $("#map").hide();
        }
    })

    /**
     * Initially set map to be hidden.
     */
    $("#map").hide();
});
